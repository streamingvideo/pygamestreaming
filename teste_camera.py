import cv2

cap = cv2.VideoCapture(0)

if not cap.isOpened():
    print("A câmera não pôde ser iniciada.")
else:
    print("A câmera está funcionando.")

cap.release()
