#UDP
import socket

def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('google.com', 0))
    return s.getsockname()[0]

def receive_video(ip, port):
    port_number = 12003 
    client_ports = {}
    socket_received = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    socket_received.bind((ip, port))
    print("Aguardando pacotes de vídeo...")
    while True:
        video_data, client_address = socket_received.recvfrom(1000000)
        if not video_data:
            break

        print(f"Recebido {len(video_data)} bytes de vídeo de {client_address[0]}:{client_address[1]}")

        send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        if client_address[0] in client_ports:
            port = client_ports[client_address[0]]
        else:
            port = port_number
            client_ports[client_address[0]] = port
            port_number += 1

        clients = list(client_ports.keys())
        for client in clients:
            send_socket.sendto(video_data, (client, port))

        print(f"Enviado {len(video_data)} bytes de vídeo para {client_address[0]}:{port}")
    return video_data, client_address

if __name__ == '__main__':
    port_received = 12000
    ip = get_ip_address()
    video_data, client_address = receive_video(ip, port_received)

# #TCP
# import socket

# def receive_video(ip, port):
#     socket_received = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     socket_received.bind((ip,port))
#     socket_received.listen(5)
#     client_socket, client_address = socket_received.accept() #only STREAM
#     print(f"Conexão estabelecida com {client_address[0]}:{client_address[1]}")
#     while True:
#         video_data = client_socket.recvfrom(1000000)
#         if not video_data:
#             break   
#         print(f"Recebido {len(video_data)} bytes de vídeo do cliente")
#     return client_address[0], video_data

# def send_video(video_data, client_address):
#     send_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     send_socket.connect((client_address[0], 12001))
#     send_socket.sendall(video_data)
#     print(f"Enviado {len(video_data)} bytes de vídeo para o cliente")

# if __name__ == '__main__':
#     port_received = 12000
#     ip = '192.168.172.27'
#     video_data, client_address = receive_video(ip, port_received)
#     send_video(video_data, client_address)
