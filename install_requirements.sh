#!/bin/bash

python3 -m pip install --upgrade pip
pip3 install pygame
pip3 install numpy
pip3 install opencv-python
pip3 install pickle
