import cv2, socket, pickle, threading, sys, pygame

def close_camera(cap):
    cv2.destroyAllWindows()
    cap.release()

def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('google.com', 0))
    return s.getsockname()[0]

def send_streaming(s, cap, server_ip, server_port):
    while True:
        ret, photo = cap.read()
        ret, buffer = cv2.imencode(".jpg", photo, [int(cv2.IMWRITE_JPEG_QUALITY), 30])
        x_as_bytes = pickle.dumps(buffer)
        s.sendto((x_as_bytes), (server_ip, server_port))
        if cv2.waitKey(10) == 13:
            break
    close_camera(cap)

def receive_video(s, ip, port, screen_num):
    s.bind((ip, port))
    while True:
        x = s.recvfrom(1000000)
        data = x[0]
        data = pickle.loads(data)
        data = cv2.imdecode(data, cv2.IMREAD_COLOR)
        data = cv2.resize(data, (150, 110))
        cv2image = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
        pygame_image = pygame.image.frombuffer(cv2image.tobytes(), cv2image.shape[1::-1], "RGB")

        # Posição dos vídeos na tela
        screen.blit(pygame_image, (160 * (screen_num - 1), 0))

        pygame.display.flip()
        if cv2.waitKey(10) == 13:
            break
    cv2.destroyAllWindows()

if __name__ == '__main__':
    server_ip = input("Digite o ip do server: ")
    server_port = 12000
    ip = get_ip_address()
    port = 12003
    amount_clients = 10

    # Inicialização da câmera
    cap = cv2.VideoCapture(-1)

    # Envio do streaming de vídeo para o servidor
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    threading.Thread(target=send_streaming, args=(s, cap, server_ip, server_port)).start()

    # Inicialização do Pygame
    pygame.init()
    screen = pygame.display.set_mode((640, 480))

    # Recebimento de streamings de vídeo vindos do servidor
    sockets = []
    for i in range(amount_clients):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sockets.append(s)
    for i in range(amount_clients):
        t = threading.Thread(target=receive_video, args=(sockets[i], ip, port, i + 1))
        t.start()
        port += 1

    # Sair
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()




    # s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # s.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 1000000)
    # server_ip = "192.168.172.27"
    # server_port = 12000
    # cap = cv2.VideoCapture(-1)
    # threading.Thread(target=send_streaming, args=(s, cap, server_ip, server_port)).start()











# # UDP
# import pygame
# import cv2, socket, pickle, threading, sys

# def get_ip_address():
#     s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     s.connect(('google.com', 0))
#     return s.getsockname()[0]

# def receive_video(s, ip, port, screen_num):
#     s.bind((ip, port))
#     while True:
#         x = s.recvfrom(1000000)
#         data = x[0]
#         data = pickle.loads(data)
#         data = cv2.imdecode(data, cv2.IMREAD_COLOR)
#         data = cv2.resize(data, (150, 110))  # adjust the resolution
#         cv2image = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
#         pygame_image = pygame.image.frombuffer(cv2image.tostring(), cv2image.shape[1::-1], "RGB")

#         # Position the screen based on the screen number
#         if screen_num == 1:
#             screen.blit(pygame_image, (0, 0))
#         elif screen_num == 2:
#             screen.blit(pygame_image, (160, 0))
#         elif screen_num == 3:
#             screen.blit(pygame_image, (320, 0))

#         pygame.display.flip()
#         if cv2.waitKey(10) == 13:
#             break
#     cv2.destroyAllWindows()

# if __name__ == '__main__':
#     s1 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     s2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     s3 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     ip = get_ip_address()
#     port1 = 12003
#     port2 = 12004
#     port3 = 12005

#     pygame.init()
#     screen = pygame.display.set_mode((640, 480))

#     # Create a thread for each incoming connection
#     threading.Thread(target=receive_video, args=(s1, ip, port1, 1)).start()
#     threading.Thread(target=receive_video, args=(s2, ip, port2, 2)).start()
#     threading.Thread(target=receive_video, args=(s3, ip, port3, 3)).start()

#     while True:
#         for event in pygame.event.get():
#             if event.type == pygame.QUIT:
#                 pygame.quit()
#                 sys.exit()

# #TCP
# import pygame
# import cv2, socket, pickle, threading, sys

# def receive_video(s, ip, port, screen_num):
#     ip = '192.168.172.27'
#     port = 12001
#     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     s.bind((ip,port))
#     s.listen(1)
#     conn, addr = s.accept()

#     while True:
#         data = b''
#         payload_size = struct.calcsize("L")
#         while True:
#             packed_msg_size = conn.recv(payload_size)
#             msg_size = struct.unpack("L", packed_msg_size)[0]
#             while len(data) < msg_size:
#                 data += conn.recv(4096)

#             data = pickle.loads(data)
#             data = cv2.imdecode(data, cv2.IMREAD_COLOR)
#             data = cv2.resize(data, (150, 110)) # adjust the resolution
#             cv2image = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
#             pygame_image = pygame.image.frombuffer(cv2image.tostring(), cv2image.shape[1::-1], "RGB")

#             # Position the screen based on the screen number
#             if screen_num == 1:
#                 screen.blit(pygame_image, (0,0))
#             elif screen_num == 2:
#                 screen.blit(pygame_image, (160,0))
#             elif screen_num == 3:
#                 screen.blit(pygame_image, (320,0))

#             pygame.display.flip()
#             if cv2.waitKey(10) == 13:
#                 break
#         cv2.destroyAllWindows()

# s1 = socket.socket(socket.AF_INET , socket.SOCK_STREAM)
# ip = '192.168.172.27'
# port1 = 12001

# pygame.init()
# screen = pygame.display.set_mode((640, 480))

# # Create a thread for each incoming connection
# threading.Thread(target=receive_video, args=(s1, ip, port1, 1)).start()

# while True:
#     for event in pygame.event.get():
#         if event.type == pygame.QUIT:
#             pygame.quit()
#             sys.exit()
